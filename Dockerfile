FROM node:10

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
COPY nodemon.json .
COPY tsconfig.json .
COPY tslint.json .

RUN npm install

EXPOSE 8080
CMD [ "npm", "start" ]

